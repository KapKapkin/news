from django.urls import path

from .views import (
    ArticleListView, 
    ArticleDeleteView,
    ArtiсleDetailView,
    ArticleUpdateView,
    ArticleCreateView,
    )

urlpatterns = [
    path('<int:pk>/edit/',
        ArticleUpdateView.as_view(), name='article_edit'),
    path('<int:pk>/',
        ArtiсleDetailView.as_view(), name='article_detail'),
    path('<int:pk>/delete/', 
        ArticleDeleteView.as_view(), name='article_delete'),

    path('', ArticleListView.as_view(), name = 'article_list'),
    path('new/', ArticleCreateView.as_view(), name = 'article_new')
]